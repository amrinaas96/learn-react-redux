import React from "react";
import ReactDOM from "react-dom";
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import "Styles/Main.scss";
import App from "./App";

//initial state
const globalState = {
  favoritePokemon: [],
};

//reducer
const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "ADD_FAVORITE":
      return {
        ...state,
        favoritePokemon = state.favoritePokemon.concat(action.newValue),
      };
    default:
      break;
  }
  return state;
}

//store
const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={storeRedux}>
       <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
